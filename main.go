package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/security-products/go-ast-scanner-launcher/command"
)

func main() {
	app := command.App()
	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
