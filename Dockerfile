FROM scratch
COPY go-ast-scanner-launcher /go-ast-scanner-launcher
ENTRYPOINT ["/go-ast-scanner-launcher"]
