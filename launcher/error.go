package launcher

const (
	errMsgCannotGetAppDirAbsPath         = "Cannot get absolute path of application directory"
	errMsgCannotCreateDockerClient       = "Cannot create docker client"
	errMsgCannotPullDockerImage          = "Cannot pull docker image"
	errMsgCannotDisplayPullImage         = "Cannot display JSON messages when pullling docker image"
	errMsgCannotCreateDockerContainer    = "Cannot create docker image"
	errMsgCannotCreateTarArchiveOfAppDir = "Cannot create tar archive of application dir"
	errMsgCannotCopyAppDirToContainer    = "Cannot copy application dir to container"
	errMsgCannotStartDockerContainer     = "Cannot start docker image"
	errMsgCannotOpenContainerLogs        = "Cannot open container logs"
	errMsgCannotCopyContainerLogs        = "Cannot copy container logs"
	errMsgCannotRemoveContainer          = "Cannot remove container"
	errMsgErrorWaitingDockerContainer    = "Error when waiting docker container"
	errMsgContainerExitWithError         = "Container exited with error"
	errMsgContainerExitWithNonZeroStatus = "Container exited with non zero exit code"
	errMsgCannotCopyFromContainer        = "Cannot copy from container"
	errMsgCannotReadTarArchive           = "Cannot read tar archive"
	errMsgMissingOutputFile              = "Output file missing from tar archive"
)

// wrapError wraps an error with an error message.
func wrapError(msg string, wrapped error) *WrappedError {
	return &WrappedError{msg, wrapped}
}

// wrapError holds an error and a contextual error message.
type WrappedError struct {
	Message string
	Wrapped error
}

func (e WrappedError) Error() string {
	return e.Message + ": " + e.Wrapped.Error()
}
