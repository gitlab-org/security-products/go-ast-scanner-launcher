package launcher

import (
	"archive/tar"
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/docker/docker/pkg/term"
	log "github.com/sirupsen/logrus"
)

const appDirDestPath = "/tmp/src/app"
const imageRef = "registry.gitlab.com/gitlab-org/security-products/analyzers/go-ast-scanner:latest"
const outputPath = "/tmp/out/go-ast-scanner-report.json"
const timeout = 20 * time.Minute

type Output struct {
	io.Reader
	Filename string
}

func GenerateFrom(appPath string) (*Output, error) {

	// Docker client
	cli, err := client.NewEnvClient()
	if err != nil {
		return nil, wrapError(errMsgCannotCreateDockerClient, err)
	}

	// Shared context
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	cli.NegotiateAPIVersion(ctx)

	// Pull Docker image
	if reader, err := cli.ImagePull(ctx, imageRef, types.ImagePullOptions{}); err != nil {
		return nil, wrapError(errMsgCannotPullDockerImage, err)
	} else {
		defer reader.Close()

		// Display output of image pull to stderr
		termFd, isTerm := term.GetFdInfo(os.Stderr)
		if err := jsonmessage.DisplayJSONMessagesStream(reader, os.Stderr, termFd, isTerm, nil); err != nil {
			return nil, wrapError(errMsgCannotDisplayPullImage, err)
		}
	}

	// directories
	appDir, err := filepath.Abs(appPath)
	if err != nil {
		return nil, wrapError(errMsgCannotGetAppDirAbsPath, err)
	}

	// Create container
	cfg := &container.Config{
		Image: imageRef,
		Cmd:   []string{appDirDestPath, filepath.Dir(outputPath)},
	}
	hostCfg := &container.HostConfig{}
	dockerContainerName := "" // avoid conflicting names
	resp, err := cli.ContainerCreate(ctx, cfg, hostCfg, nil, dockerContainerName)
	if err != nil {
		return nil, wrapError(errMsgCannotCreateDockerContainer, err)
	}

	// Copy logs and remove container on exit
	defer func() {
		// copy logs to stderr
		logsOpts := types.ContainerLogsOptions{
			ShowStdout: true,
			ShowStderr: true,
			Follow:     true,
		}
		if reader, err := cli.ContainerLogs(ctx, resp.ID, logsOpts); err != nil {
			log.Warn(errMsgCannotOpenContainerLogs, err)
		} else {
			if _, err := stdcopy.StdCopy(os.Stderr, os.Stderr, reader); err != nil {
				log.Warn(errMsgCannotCopyContainerLogs, err)
			} else {
				reader.Close()
			}
		}

		// remove container
		removeOpts := types.ContainerRemoveOptions{
			RemoveVolumes: true,
			Force:         true,
		}
		if err := cli.ContainerRemove(ctx, resp.ID, removeOpts); err != nil {
			log.Warn(errMsgCannotRemoveContainer)
		}
	}()

	// Copy application directory to container using a tar archive.
	// Destination path must exist but directories listed in the tar archive are automatically created,
	// so this is why appDirDestPath is used as a prefix when adding the files to the archive.
	copyBuf := new(bytes.Buffer)
	if err := Tar(appDir, appDirDestPath, copyBuf); err != nil {
		return nil, wrapError(errMsgCannotCreateTarArchiveOfAppDir, err)
	}
	if err := cli.CopyToContainer(ctx, resp.ID, "/", copyBuf, types.CopyToContainerOptions{}); err != nil {
		return nil, wrapError(errMsgCannotCopyAppDirToContainer, err)
	}

	// Start image
	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, wrapError(errMsgCannotStartDockerContainer, err)
	}

	// Wait until the container installs the necessary tools and list dependencies
	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNextExit)
	select {
	case err := <-errCh:
		if err != nil {
			return nil, wrapError(errMsgErrorWaitingDockerContainer, err)
		}
	case waitOK := <-statusCh:
		if waitOK.Error != nil {
			return nil, wrapError(errMsgContainerExitWithError, err)
		}
		if waitOK.StatusCode != 0 {
			return nil, fmt.Errorf("%s: %d", errMsgContainerExitWithNonZeroStatus, waitOK.StatusCode)
		}
	}

	// Get output file via a tar archive
	outputDir := filepath.Dir(outputPath)
	archive, _, err := cli.CopyFromContainer(ctx, resp.ID, outputDir)
	if err != nil {
		return nil, wrapError(errMsgCannotCopyFromContainer, err)
	}
	defer archive.Close()

	// Extract output file from archive
	outputFilename := filepath.Base(outputPath)
	reader := tar.NewReader(archive)
loop:
	for {
		header, err := reader.Next()
		switch err {
		case nil:
			if filepath.Base(header.Name) == outputFilename {
				// Return output
				buf := new(bytes.Buffer)
				if _, err = io.Copy(buf, reader); err != nil {
					return nil, err
				}
				return &Output{buf, outputFilename}, nil
			}
		case io.EOF:
			break loop
		default:
			return nil, wrapError(errMsgCannotReadTarArchive, err)
		}
	}

	// Expected output file not found
	return nil, wrapError(errMsgMissingOutputFile, err)
}
