# Go AST Scanner launcher

GitLab analyzer launcher to integrate the [Go AST Scanner analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/go-ast-scanner) into GitLab [SAST](https://gitlab.com/gitlab-org/security-products/sast).

## How to use

1. Download a binary release or build from sources
1. Run the binary:

    ```sh
    ./go-ast-scanner-launcher analyze /path/to/source/code > ./go-ast-scanner-report.json
    ```

1. The results will be stored in the `go-ast-scanner-report.json` file in the current directory.

## Versioning and release process

TODO

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
