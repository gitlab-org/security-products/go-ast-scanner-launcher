package command

import (
	"errors"
	"io"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/go-ast-scanner-launcher/launcher"
)

const IndentJSONFlag = "indent"

var (
	ErrWrongNumberOfArgs = errors.New("Wrong number of args")
)

func Analyze() cli.Command {

	return cli.Command{
		Name:      "analyze",
		Usage:     "Run the analyzer and ouptut report to stdout",
		ArgsUsage: "<directory>",
		Action: func(c *cli.Context) error {
			if !c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs
			}

			// launch analyze on given application path
			appPath := c.Args().First()
			out, err := launcher.GenerateFrom(appPath)
			if err != nil {
				return err
			}

			// render report on stdout
			io.Copy(os.Stdout, out.Reader)

			return nil
		},
	}
}
